using System;
using System.Collections.Generic;
using System.Text;
using PriceCalc.Ent;

namespace PriceCalc.Comp
{
	public static class Printer
	{
		public static void Print(string Message)
		{
			Console.WriteLine(Message);
		}

		public static string Receipt(decimal Subtotal, decimal Total, List<string> Offers)
		{
			var receipt = new StringBuilder();
			receipt.AppendFormat("Subtotal: {0}", Subtotal.ToString("C", Common.enGB));
			receipt.AppendLine();

			if ((Offers?.Count ?? 0) > 0)
			{
				receipt.AppendJoin(Environment.NewLine, Offers);
			}
			else
			{
				receipt.Append("(No offers available)");
			}
			receipt.AppendLine();

			receipt.AppendFormat("Total: {0}", Total.ToString("C", Common.enGB));
			receipt.AppendLine();

			return receipt.ToString();
		}

		public static string ProductDiscountOffer(string ProductName, decimal Percent, decimal Value)
		{
			var value = Value.ToString("C", Common.enGB);
			return $"{ProductName} {Percent}% off: {value}";
		}

		public static string MessageInvalidArgs(IEnumerable<string> inventoryList)
		{
			var msg = new StringBuilder();
			msg.AppendLine("There are items in your basket that are not in inventory.");
			msg.AppendLine("The current inventory is: ");
			msg.AppendJoin(',', inventoryList);

			return msg.ToString();
		}
	}
}