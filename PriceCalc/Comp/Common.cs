using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.Json;
using PriceCalc.Ent;

namespace PriceCalc.Comp
{
	public static class Common
	{
		public const string ARGS = "args";

		const string BasePath = @"C:\Projetcs\edv\PriceCalc\";
		static string inventorySource => Path.Combine(BasePath, "inventory.json");

		public static CultureInfo enGB => CultureInfo.GetCultureInfo("en-GB");



		public static Dictionary<string, Product> GetInventory()
		{
			var products = new Dictionary<string, Product>();

			var opt = new JsonDocumentOptions
			{
				AllowTrailingCommas = true,
				CommentHandling = JsonCommentHandling.Skip
			};

			var invJson = File.ReadAllText(inventorySource);

			using (var document = JsonDocument.Parse(invJson, opt))
			{
				foreach (var product in document.RootElement.EnumerateArray())
				{
					var name = product.GetProperty("Name").GetString();
					var price = product.GetProperty("Price").GetDecimal();
					var um = product.GetProperty("UM").GetString();

					var p = new Product()
					{
						Name = name,
						Price = price,
						UM = um
					};

					if (product.TryGetProperty("Offer", out var offer))
					{
						var offerType = offer.GetProperty("Type").GetString();

						if (offerType == "%")
						{
							var percent = offer.GetProperty("Percent").GetDecimal();

							var o = new ProductDiscountOffer(p, percent);

							p.Offer = o;
						}
					}

					products.Add(p.Name.ToLower(), p);
				}
			}

			return products;
		}

	}
}