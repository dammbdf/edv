using System;
using System.Collections.Generic;
using System.Linq;
using PriceCalc.Ent;

namespace PriceCalc.Comp
{
	public class Receipt
	{
		public Basket Basket { get; private set; }

		public decimal Subtotal =>
			Basket.Products.Aggregate(0m, (current, next) => current += next.Price);
		public decimal OfferValues =>
			Basket.Products.Where(p => p.Offer != null).Select(p => p.Offer)
				.Aggregate(0m, (current, next) => current += next.Value());
		public decimal Total =>
			Subtotal + OfferValues;
		public List<string> OfferText =>
			Basket.Products.Where(p => p.Offer != null).Select(p => p.Offer.ToString()).ToList();



		public Receipt(Basket Basket)
		{
			if (Basket == null)
			{
				throw new ArgumentNullException();
			}

			this.Basket = Basket;
		}



		public override string ToString()
		{
			return Printer.Receipt(Subtotal, Total, OfferText);
		}
	}
}