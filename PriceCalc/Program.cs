﻿using System;
using System.Collections.Generic;
using System.Linq;
using PriceCalc.Comp;
using PriceCalc.Ent;

namespace PriceCalc
{
	public class Program
	{
		// --- Fields & Properties --------------
		public static Dictionary<string, Product> inventory = Common.GetInventory();

		// --- EP -------------------------------
		public static void Main(string[] args)
		{
			Console.WriteLine("__>");

			var basket = GetBasket(args);

			var receipt = new Receipt(basket);

			Console.WriteLine(receipt.ToString());
		}

		// --- Methods --------------------------

		public static Basket GetBasket(string[] args)
		{
			var basket = new Basket();

			try
			{
				basket.Products = GetProducts(args, inventory);
			}
			catch (ArgumentException ex)
			{
				if (ex.ParamName == Common.ARGS)
				{
					var inventoryList = inventory.Values.Select(i => i.Name);
					Console.WriteLine(Printer.MessageInvalidArgs(inventoryList));
				}
				else
				{
					Console.WriteLine(ex.Message);
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}

			return basket;
		}
		public static List<Product> GetProducts(string[] args, Dictionary<string, Product> inventory)
		{
			var products = new List<Product>();

			foreach (var arg in args)
			{
				var argLower = arg.ToLower();

				if (!inventory.ContainsKey(argLower))
				{
					throw new ArgumentException($"Unknown argument: {arg}", Common.ARGS);
				}

				products.Add(inventory[argLower]);
			}

			return products;
		}
	}
}
