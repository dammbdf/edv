namespace PriceCalc.Ent
{
	public interface IOffer
	{
		decimal Value();
	}
}