namespace PriceCalc.Ent
{
	public class Product
	{
		// --- Fields & Properties
		public string Name { get; set; }
		public decimal Price { get; set; }
		public string UM { get; set; }
		public IOffer Offer { get; set; }
	}
}