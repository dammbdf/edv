using System;
using PriceCalc.Comp;

namespace PriceCalc.Ent
{
	public class ProductDiscountOffer : IOffer
	{
		// --- Implementation -------------------
		public decimal Value()
		{
			return DiscountValue(Product.Price, Percent);
		}

		// --- Fields & Properties --------------
		Product Product { get; set; }
		public decimal Percent { get; set; } = 0m;

		// --- CTORs ----------------------------
		public ProductDiscountOffer(Product Product)
		{
			this.Product = Product;
		}
		public ProductDiscountOffer(Product Product, decimal Percent) : this(Product)
		{
			this.Percent = Percent;
		}

		// --- Methods --------------------------
		public decimal DiscountValue(decimal Price, decimal Percent)
		{
			decimal discountValue = Price * (Percent / 100);
			discountValue = Math.Round(discountValue, 2, MidpointRounding.AwayFromZero);
			discountValue = -discountValue;

			return discountValue;
		}

		public override string ToString()
		{
			return Printer.ProductDiscountOffer(Product.Name, Percent, Value());
		}
	}
}