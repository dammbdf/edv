using System;
using System.Collections.Generic;

namespace PriceCalc.Ent
{
	public class Basket
	{
		// --- Fields & Properties --------------
		private List<Product> products = new List<Product>();
		public List<Product> Products
		{
			get { return products; }
			set { products = value; }
		}

		private List<IOffer> offers;
		public List<IOffer> Offers
		{
			get { return offers; }
			set { offers = value; }
		}

		// --- CTORs ----------------------------

		// --- Methods --------------------------

	}
}