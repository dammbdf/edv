using System.Collections.Generic;
using Xunit;
using PriceCalc.Comp;
using PriceCalc.Ent;

namespace PriceCalcTests.Comp
{
	public class ReceiptTests
	{
		[Fact]
		public void Totals()
		{
			//Given
			var basket = new Basket();

			var p = new Product { Name = "Tp1", Price = 10m };
			var o = new ProductDiscountOffer(p, 10m);
			p.Offer = o;
			basket.Products.Add(p);

			p = new Product { Name = "Tp2", Price = 2.2m };
			o = new ProductDiscountOffer(p, 50m);
			p.Offer = o;
			basket.Products.Add(p);

			//When
			var pricer = new Receipt(basket);

			//Then
			Assert.Equal(12.2m, pricer.Subtotal);
			Assert.Equal(-2.1m, pricer.OfferValues);
			Assert.Equal(10.1m, pricer.Total);
		}

		[Fact]
		public void ToString_NoOfferLines()
		{
			//Given
			var expected = "Subtotal: £5.50\r\n(No offers available)\r\nTotal: £5.50\r\n";

			var Subtotal = 5.5m;
			var Total = 5.5m;
			var Offers = (List<string>)null;

			//When
			var actual = Printer.Receipt(Subtotal, Total, Offers);

			//Then
			Assert.Equal(expected, actual);
		}

		[Fact]
		public void ToString_WithOfferLines()
		{
			//Given
			var expected = "Subtotal: £10.00\r\nline 1\r\nline 2\r\nTotal: £11.50\r\n";

			var Subtotal = 10m;
			var Total = 11.5m;
			var Offers = new List<string>();
			Offers.Add("line 1");
			Offers.Add("line 2");

			//When
			var actual = Printer.Receipt(Subtotal, Total, Offers);

			//Then
			Assert.Equal(expected, actual);
		}

	}
}