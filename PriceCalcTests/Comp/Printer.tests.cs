using System.Collections.Generic;
using Xunit;
using PriceCalc.Comp;

namespace PriceCalcTests.Comp
{
	public class PrinterTests
	{
		[Fact]
		public void Receipt_NoOfferLines()
		{
			//Given
			var expected = "Subtotal: £5.50\r\n(No offers available)\r\nTotal: £5.50\r\n";

			var Subtotal = 5.5m;
			var Total = 5.5m;
			var Offers = (List<string>)null;

			//When
			var actual = Printer.Receipt(Subtotal, Total, Offers);

			//Then
			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Receipt_WithOfferLines()
		{
			//Given
			var expected = "Subtotal: £10.00\r\nline 1\r\nline 2\r\nTotal: £11.50\r\n";

			var Subtotal = 10m;
			var Total = 11.5m;
			var Offers = new List<string>();
			Offers.Add("line 1");
			Offers.Add("line 2");

			//When
			var actual = Printer.Receipt(Subtotal, Total, Offers);

			//Then
			Assert.Equal(expected, actual);
		}


		[Fact]
		public void MessageInvalidArgs()
		{
			var expected = "There are items in your basket that are not in inventory.\r\nThe current inventory is: \r\nBeans,Bread,Milk,Apples";
			var inventory = new string[] { "Beans", "Bread", "Milk", "Apples" };

			var actual = Printer.MessageInvalidArgs(inventory);

			Assert.Equal(expected, actual);
		}
	}
}