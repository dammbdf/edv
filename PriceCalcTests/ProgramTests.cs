using System;
using System.Collections.Generic;
using Xunit;

using PriceCalc;
using PriceCalc.Ent;

namespace PriceCalcTests
{
	public class ProgramTests
	{

		Dictionary<string, Product> GetTestInventory()
		{
			var inventory = new Dictionary<string, Product>();

			var p = new Product { Name = "Beans" };
			inventory.Add(p.Name.ToLower(), p);
			p = new Product { Name = "Bread" };
			inventory.Add(p.Name.ToLower(), p);
			p = new Product { Name = "Milk" };
			inventory.Add(p.Name.ToLower(), p);
			p = new Product { Name = "Apples" };
			inventory.Add(p.Name.ToLower(), p);

			return inventory;
		}

		[Fact]
		public void ValidateArgs_InvalidArgs_False()
		{
			var inventory = GetTestInventory();
			var args = new string[] { "a1", "a2", "MiLK" };

			Assert.Throws<ArgumentException>(() => Program.GetProducts(args, inventory));
		}
		[Fact]
		public void ValidateArgs_ValidArgs_True()
		{
			var inventory = GetTestInventory();
			var args = new string[] { "Beans", "apples", "MiLK" };

			var actual = Program.GetProducts(args, inventory);

			Assert.NotEqual<int>(0, actual.Count);
		}

		[Fact]
		public void MainTest()
		{
			//var args = new string[] { };
			//var args = new string[] { "NA" };
			var args = new string[] { "Beans", "apples", "MiLK" };

			Program.Main(args);
		}
	}
}
