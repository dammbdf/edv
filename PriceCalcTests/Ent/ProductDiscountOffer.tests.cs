using System;
using Xunit;

using PriceCalc.Ent;

namespace PriceCalcTests.Ent
{
	public class ProductDiscountOfferTests
	{
		[Theory]
		[InlineData(10, 0, 0)]
		[InlineData(10, 100, -10)]
		[InlineData(10, 10, -1)]
		[InlineData(10, -10, 1)]
		[InlineData(45, 50, -22.5)]
		[InlineData(45, 10.1, -4.55)]
		public void Apply(decimal Price, decimal Percent, decimal Expected)
		{
			//Given
			var p = new Product()
			{
				Name = "test",
				Price = Price
			};
			var pdo = new ProductDiscountOffer(p, Percent);

			//When
			var actual = pdo.Value();

			//Then
			Assert.Equal(Expected, actual);
		}

	}
}